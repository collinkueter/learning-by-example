/**
 * Idea taken from Fun Fun Function
 *
 * Inheritance is designing your objects around what they are (is a relationship)
 * This is created in contrast to composition which you can find in the composition.js file
 * This example is designed to show how composition is a better design pattern that is more flexible
 * and does not require you to know the future. Looking at the commit history is helpful in understanding
 * how we go to the final solution
 *
 * A Product Owner gives you these requirements in this order, spread out by weeks
 *
 * dog            = pooper + barker
 * cat            = pooper + meower
 * cleaningRobot  = driver + cleaner
 * doctorRobot    = driver + healer
 * doctorRobotDog = driver + healer + barker
 *
 * The process to accomplish each will be as follows:
 * 1. create an object around what it is
 * 2. if a similar type of object exist with identical methods, split into parent class and inherit
 * 3. create an instance of the object and use it's capabilities
 *
 **/

// Now refactor cat and dog to use a parent Animal class
class Animal {
  constructor(name) {
    this.name = name;
  }
  poop() {
    console.log(this.name, "took a big 💩");
  }
}

class Dog extends Animal {
  bark() {
    console.log(this.name, "says Woof, Woof");
  }
}

class Cat extends Animal {
  meow() {
    console.log(this.name, "says Meow, Meow");
  }
}

let fido = new Dog("Fido");
fido.bark();
fido.poop();

let jeff = new Cat("Jeff");
jeff.meow();
jeff.poop();

class Robot {
  constructor(name, target) {
    this.name = name;
    this.target = target;
    this.position = 0;
    this.speed = 100;
  }
  drive() {
    let newPosition = this.position + this.speed;
    console.log(this.name, "moved from", this.position, "to", newPosition);
    this.position = newPosition;
  }
}

class CleaningRobot extends Robot {
  clean() {
    console.log(this.name, "cleaned the area");
  }
}

class DoctorRobot extends Robot {
  heal() {
    console.log(this.name, "healed", this.target);
  }
}

let larry = new CleaningRobot("Larry");
larry.drive();
larry.clean();
larry.drive();
larry.clean();

let drDre = new DoctorRobot("Dr. Dre", "Jeff");
drDre.heal();
drDre.drive();

// Now we must create a DoctorRobotDog...this is where the inheritance pattern breaks down..
/**
 * Option 1: for languages that allow, inherit from animal and robot. The issues is you now have 
 *  methods that are not allowed for a robot dog (poop)
 * 
 * Option 2: TBD
 */

 const obj = {
   find: () => {
     let vari = "I'm private";
     return "public return";
   }
 }

 const cl = function() {
   const f = 'fly'
   return function fly() {
     return f;
   }
 }
console.log(cl()());