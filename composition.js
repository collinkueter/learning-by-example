/**
 * Idea taken from Fun Fun Function
 *
 * Composition is designing your objects around what they do (has a relationship)
 * This is created in contrast to inheritance which you can find in the inheritance.js file
 *
 * A Product Owner gives you these requirements in this order, spread out by weeks
 *
 * dog            = pooper + barker
 * cat            = pooper + meower
 * cleaningRobot  = driver + cleaner
 * doctorRobot    = driver + healer
 * doctorRobotDog = driver + healer + barker
 *
 * The process to accomplish each will be as follows:
 * 1. create (or reuse) the capabilities the object will do
 * 2. create the object definition
 * 3. create an instance of the object and use it's capabilities
 *
 **/

// Dog
const barker = state => ({
  bark: () => console.log(state.name, "says Woof, Woof")
});
const pooper = state => ({
  poop: () => console.log(state.name, "took a big 💩")
});
const dog = name => {
  let state = {
    name
  };
  return Object.assign({}, barker(state), pooper(state));
};

let fido = dog("Fido");
fido.bark();
fido.poop();

// Cat
const meower = state => ({
  meow: () => console.log(state.name, "says Meow, Meow")
});
const cat = name => {
  let state = {
    name
  };
  return Object.assign({}, meower(state), pooper(state)); // We can reuse poop from above so no need to recreate
};

let jeff = cat("Jeff");
jeff.meow();
jeff.poop();

// Cleaning Robot
const cleaner = state => ({
  clean: () => console.log(state.name, "cleaned the area")
});
const driver = state => ({
  drive: () => {
    let newPosition = state.position + state.speed;
    console.log(state.name, "moved from", state.position, "to", newPosition);
    state.position = newPosition;
  }
});
const cleaningRobot = name => {
  let state = {
    name,
    speed: 100,
    position: 0
  };
  return Object.assign({}, cleaner(state), driver(state));
};
let larry = cleaningRobot("Larry");
larry.drive();
larry.clean();
larry.drive();
larry.clean();

// Doctor Robot
const healer = state => ({
  heal: () => console.log(state.name, "healed", state.target)
});

const doctorRobot = (name, target) => {
  let state = {
    name,
    target,
    speed: 100,
    position: 0
  };
  return Object.assign({}, healer(state), driver(state));
};
let drDre = doctorRobot("Dr. Dre", "Jeff");
drDre.heal();
drDre.drive();

// Doctor Robot Dog
const doctorRobotDog = (name, target) => {
  let state = {
    name,
    target,
    speed: 100,
    position: 0
  };
  return Object.assign({}, healer(state), driver(state), barker(state));
};

let sniffles = doctorRobotDog("sniffles", "fido");
sniffles.bark();
sniffles.drive();
sniffles.heal();
